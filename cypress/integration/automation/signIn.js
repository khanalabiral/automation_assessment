context('Logging into the account', () => {

    it('.login() - Logging into the automationPractice account', () => {
        cy.visit('http://www.automationpractice.com')
        cy.get('.login')
          .click()
        cy.get('input[id="email"]')
          .type('abiral@yopmail.com')
        cy.get('input[name="passwd"]')
          .type('12345') 
        cy.get('button[name="SubmitLogin"]')
          .click()
        if(cy.title().should('include', 'My account - My Store'))
        {
            cy.log('Test is passed')
        }
        else
        {
            cy.console.error('Test failed');
        } 
    })
}) 