import './signIn'
context('Adding product to cart,checkout and payment', () => {

    it('.addingProductsToCart() - Adding two products to cart, checking out and proceeding to payment', () => {
        cy.get('#block_top_menu > ul > li:nth-child(2)')
          .click()
        cy.get('#categories_block_left > div > ul > li:nth-child(1) > a')
          .click()
        cy.get('#center_column > ul > li > div > div.left-block > div > a.product_img_link > img')
          .click()
        cy.get('#add_to_cart > button')
          .click()
        if(cy.contains('Product successfully added to your shopping cart').should('be.visible') )
          {
            cy.log('Test is passing')
          }
        cy.get('#layer_cart > div.clearfix > div > div.button-container > span > span')
          .click()
        cy.get('#block_top_menu > ul > li:nth-child(3)')
          .click()
        cy.get('#center_column > ul > li > div > div.left-block > div > a.product_img_link > img')
          .click()
        cy.get('#add_to_cart > button > span')
          .click()
        cy.get('#layer_cart > div.clearfix > div > div.button-container > a > span')
          .click()
        if(cy.contains('Shopping-cart summary').should('be.visible'))
          {
            cy.log('Test is passing')
          }
        cy.get('#center_column > p > a.button')
          .click()
        cy.get('input[id="email"]')
          .type('abiral@yopmail.com')
        cy.get('input[name="passwd"]')
          .type('12345') 
        cy.get('button[name="SubmitLogin"]')
          .click()
        if(cy.contains('Addresses').should('be.visible') )
          {
            cy.log('Test is passing')
          }
        cy.get('textarea')
          .type('This is the message for the order')
        cy.get('#center_column > form > p > button')
          .click() 
        cy.get('#cgv')
          .click()
        cy.get('#form > p > button')
          .click()
        if(cy.contains('Please choose your payment method').should('be.visible') )
          {
            cy.log('Test is passing')
          }
        cy.get('#HOOK_PAYMENT > div:nth-child(1) > div > p > a')
          .click()
        cy.get('#cart_navigation > button')
          .click()
        if(cy.contains('Order confirmation').should('be.visible') )
          {
            cy.log('Test is passed')
          }
    })
}) 