context('Different cases of invalid sign in', () => {

    it('.invalidSignIn() - Signing in with wrong email', () => {
        cy.visit('http://www.automationpractice.com')
        cy.get('.login')
          .click()
        cy.get('input[id="email"]')
          .type('abiral1@yopmail.com')
        cy.get('input[name="passwd"]')
          .type('12345') 
        cy.get('button[name="SubmitLogin"]')
          .click()
        if(cy.contains('Authentication failed.').should('be.visible'))
        {
            cy.log('Test is passed')
        } 
    })
    it('.invalidSignIn() - Signing in with wrong password', () => {
        cy.visit('http://www.automationpractice.com')
        cy.get('.login')
          .click()
        cy.get('input[id="email"]')
          .type('abiral@yopmail.com')
        cy.get('input[name="passwd"]')
          .type('1234567') 
        cy.get('button[name="SubmitLogin"]')
          .click()
        if(cy.contains('Authentication failed.').should('be.visible'))
        {
            cy.log('Test is passed')
        }
    })
    it('.invalidSignIn() - Signing in with empty password', () => {
        cy.visit('http://www.automationpractice.com')
        cy.get('.login')
          .click()
        cy.get('input[id="email"]')
          .type('abiral1@yopmail.com')
        cy.get('button[name="SubmitLogin"]')
          .click()
        if(cy.contains('Password is required.').should('be.visible'))
        {
            cy.log('Test is passed')
        }
    })
    it('.invalidSignIn() - Signing in with empty email', () => {
        cy.visit('http://www.automationpractice.com')
        cy.get('.login')
          .click()
        cy.get('input[name="passwd"]')
          .type('12345') 
        cy.get('button[name="SubmitLogin"]')
          .click()
        if(cy.contains('An email address required.').should('be.visible'))
        {
            cy.log('Test is passed')
        }
    })
    it('.invalidSignIn() - Signing in with empty email and empty password', () => {
        cy.visit('http://www.automationpractice.com')
        cy.get('.login')
          .click() 
        cy.get('button[name="SubmitLogin"]')
          .click()
        if(cy.contains('An email address required.').should('be.visible'))
        {
            cy.log('Test is passed')
        }
    })
}) 