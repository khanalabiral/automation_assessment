context('Registering for automationpractice.com', () => {

    it('.invalidRegistration() - Registering with existing email', () => {
        cy.visit('http://www.automationpractice.com')
        cy.get('.login')
          .click()
        cy.get('input[name="email_create"]')
          .type('abiral2@yopmail.com')
        cy.get('button[name="SubmitCreate"]').click()
        cy.wait(1000)
        if(cy.contains('An account using this email address has already been registered. Please enter a valid password or request a new one.').should('be.visible') )
        {
          cy.log('Test is passed')
        }

    }) 
}) 